﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Planner.Common;
using Planner.Data;
using Planner.Presenters;
using Planner.Properties;


namespace Planner.Views
{
    public partial class MainView : Form
    {
        #region Fields

        private readonly MainPresenter _presenter;
        private string _coursesToSign;
        private Image _plan;
        private CancellationTokenSource _token;

        #endregion

        #region  Properties

        public string PathToXml
        {
            get { return pathText.Text; }
            set { pathText.Text = value; }
        }

        public string ScenarioChoice => scenarioCombo.Text;

        public string CoursesToSign
        {
            get { return _coursesToSign; }
            set
            {
                lock (this)
                {
                    _coursesToSign += value + Environment.NewLine;
                    AppendText(value);
                }
            }
        }

        public Image Plan
        {
            get { return _plan; }
            set
            {
                lock (this)
                {
                    _plan = value;
                    SetImage(value);
                }
            }
        }

        public AppConfigObj AppConfigObj { get; set; }

        public string ShowMessage
        {
            set { MessageBox.Show(value); }
        }

        public bool GenerateButVisibility
        {
            set
            {
                SetVisibility(value);
            }
        }

        #endregion

        #region Constructor(s)

        public MainView()
        {
            InitializeComponent();

            
            _presenter = new MainPresenter(this);

            Init();
        }

        #endregion

        #region Methods

        private void Init()
        {
            saveImageButton.Enabled = false;
            AppConfigObj = new AppConfigObj
            {
                IsThreadingEnabled = Settings.Default.IsThreadingEnabled,
                PathToSave = Settings.Default.PathToSaveImage
            };

            if (string.IsNullOrEmpty(AppConfigObj.PathToSave))
            {
                var dialogResult = MessageBox.Show(@"Brak docelowej ścieżki zapisu danych." + Environment.NewLine + @"Zapis może nie działać poprawnie"
+ Environment.NewLine + @"Czy pokazać okno ustawień?", @"Uwaga",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);

                if (dialogResult == DialogResult.Yes)
                {
                    ShowSettingsForm();
                }
            }
            ChangeVisibility();
        }

        private void ChangeVisibility()
        {
            if (AppConfigObj == null)
            {
                return;
            }

            saveImageButton.Visible = saveTextToFile.Visible = CancelButton.Visible = AppConfigObj.IsThreadingEnabled;
        }

        private void SetImage(Image image)
        {
            Invoke(new Action(() => pictureBox1.Image = image));
        }

        private void SetVisibility(bool visibility)
        {
            Invoke(new Action(() => generateBut.Enabled = visibility));
        }

        private void AppendText(string textToDisplay)
        {
            Invoke(new Action(() => CoursesToSignText.Text += textToDisplay + Environment.NewLine));
        }

        private void ClearView()
        {
            pathText.Text = string.Empty;
            scenarioCombo.Text = string.Empty;
            CoursesToSignText.Text = string.Empty;
            pictureBox1.Image = new Bitmap(Resources.awd_tlo);
            generateBut.Enabled = true;
        }

        private void deserializeXmlBut_Click(object sender, EventArgs e)
        {
            ClearView();
            using (Cursor.Current = Cursors.WaitCursor)
            {
                using (var dialog = new OpenFileDialog()
                {
                    Filter = @"XML Files | *.xml",
                    DefaultExt = ".xml"
                })
                {
                    if (dialog.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }
                    PathToXml = dialog.FileName;
                }
            }
        }

        private void generateBut_Click(object sender, EventArgs e)
        {
            generateBut.Enabled = false;
            saveImageButton.Enabled = true;
            if (string.IsNullOrEmpty(PathToXml) || string.IsNullOrEmpty(ScenarioChoice))
            {
                MessageBox.Show(@"Niepoprawnie uzupełnione dane!");
                generateBut.Enabled = true;
                return;
            }

            using (Cursor.Current = Cursors.WaitCursor)
            {
                _presenter.DeserializeXmlCmd(PathToXml);
                _token = new CancellationTokenSource();

                switch (ScenarioChoice)
                {
                    case "Jak najwczesniej":
                        ThreadPool.QueueUserWorkItem(
                            state => _presenter.CreatePlanCmd(SortingScenarios.AsEarlyAsPossible, _token.Token),
                            _token.Token);
                        break;
                    case "Jak najpozniej":
                        ThreadPool.QueueUserWorkItem(
                            state => _presenter.CreatePlanCmd(SortingScenarios.AsLateAsPossible, _token.Token),
                            _token.Token);
                        break;
                    case "Jak najwiecej dni wolnych":
                        MessageBox.Show(@"Nie zaimplementowano");
                        break;
                    case "Jak najmniej okienek":
                        ThreadPool.QueueUserWorkItem(
                            state => _presenter.CreatePlanCmd(SortingScenarios.NoBreaks, _token.Token), _token.Token);
                        break;
                    case "":
                        MessageBox.Show(@"Wybierz scenariusz!");
                        break;
                }
            }
        }

        private void saveTextToFile_Click(object sender, EventArgs e)
        {
            var thread = new Thread(delegate() { _presenter.SaveToFile(CoursesToSign); });
            thread.Start();
            MessageBox.Show(@"Zapisano do pliku");
        }

        private void saveImageButton_Click(object sender, EventArgs e)
        {
            var thread = new Thread(delegate() { _presenter.SaveToFile(Plan); });
            thread.Start();
            MessageBox.Show(@"Zapisano do pliku");
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            _token?.Cancel();

            ClearView();
        }

        private void generujXmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var xmlGeneratorView = new XmlGeneratorView();
            xmlGeneratorView.Show();
        }

        private void edytujXmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var xmlEditorView = new XmlEditorView();
            xmlEditorView.Show();
        }

        private void trybyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSettingsForm();
        }

        private void ShowSettingsForm()
        {
            using (var form = new AppConfig())
            {
                var result = form.ShowDialog();

                if (result == DialogResult.OK)
                {
                    AppConfigObj = form.ReturnObj;
                    ChangeVisibility();
                }
            }
        }

        #endregion
    }
}