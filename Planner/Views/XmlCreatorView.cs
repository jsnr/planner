﻿using System;
using System.Windows.Forms;
using Planner.Converters;
using Planner.Data;
using Planner.Presenters;

namespace Planner.Views
{
    public partial class XmlGeneratorView : Form
    {
        #region Fields

        private readonly XmlCreatorPresenter _presenter;
        private int _selectedRow;

        #endregion

        #region Contructor(s)

        public XmlGeneratorView()
        {
            InitializeComponent();
            _presenter = new XmlCreatorPresenter();
        }

        #endregion

        #region Methods

        private void generateXmlButton_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog
            {
                Filter = @"XML Files | *.xml",
                DefaultExt = ".xml"
            })
            {
                if (dialog.ShowDialog() != DialogResult.OK)
                    return;

                _presenter.GenerateXmlCmd(dialog.FileName);
            }

            MessageBox.Show(@"Zapisano do pliku");
        }

        private void addToListButton_Click(object sender, EventArgs e)
        {
            //TODO: wrong data input
            if (string.IsNullOrEmpty(startHourText.Text)
                || string.IsNullOrEmpty(startMinutesText.Text)
                || string.IsNullOrEmpty(dayCombo.Text))
            {
                MessageBox.Show(@"Niepoprawnie uzupełnione dane");
                return;
            }

            var obj = new ClassObj
            {
                GroupCode = groupCodeText.Text,
                CourseCode = courseCodeText.Text,
                CourseName = courseNameText.Text,
                TeacherName = teacherNameText.Text,
                RoomNumber = roomText.Text,
                Day = DayToIntConverter.ConvertStringToDay(dayCombo.Text),
                StartHour = startHourText.Text + ":" + startMinutesText.Text,
                EndHour = endHourText.Text + ":" + endMinutesText.Text,
                WhichWeek = whichWeekCombo.Text
            };
            _presenter.AddToListCmd(obj);

            RefreshGrid();
            ClearView();
        }

        private void classesGrid_SelectionChanged(object sender, EventArgs e)
        {
            _selectedRow = classesGrid.CurrentCell.RowIndex;
        }

        private void deleteItemButton_Click(object sender, EventArgs e)
        {
            var itemToDelete = classesGrid.Rows[_selectedRow].Cells[0].Value as string;
            _presenter.DeleteItemCmd(itemToDelete);
            RefreshGrid();
        }

        private void ClearView()
        {
            groupCodeText.Text = string.Empty;
            courseCodeText.Text = string.Empty;
            teacherNameText.Text = string.Empty;
        }

        private void RefreshGrid()
        {
            classesGrid.DataSource = null;
            classesGrid.DataSource = _presenter.Classes;
            classesGrid.Update();
            classesGrid.Refresh();
        }

        #endregion
    }
}