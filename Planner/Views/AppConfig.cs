﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Planner.Data;
using Planner.Properties;

namespace Planner.Views
{
    public partial class AppConfig : Form
    {
        #region Propetries

        public AppConfigObj ReturnObj { get; set; }

        #endregion

        #region Constructor(s)

        public AppConfig()
        {
            InitializeComponent();

            IsThreadingEnabled.Checked = Settings.Default.IsThreadingEnabled;
            pathToSave.Text = Settings.Default.PathToSaveImage;
        }

        #endregion

        #region Methods
        
        private void saveConfigButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(pathToSave.Text))
            {
                var dialogResult =  MessageBox.Show(
                    @"Czy zapisać zmiany z pustą ścieżką?" + Environment.NewLine +
                    @"Może to skutkować błędnym działaniem programu.", @"Uwaga!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning );

                if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }

            ReturnObj = new AppConfigObj
            {
                IsThreadingEnabled = IsThreadingEnabled.Checked,
                PathToSave = pathToSave.Text,
            };

            Settings.Default.IsThreadingEnabled = ReturnObj.IsThreadingEnabled;
            Settings.Default.PathToSaveImage = ReturnObj.PathToSave;
            Settings.Default.Save();

            DialogResult = DialogResult.OK;
            Close();
        }


        #endregion
    }
}
