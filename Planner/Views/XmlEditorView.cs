﻿using System;
using System.Windows.Forms;
using Planner.Converters;
using Planner.Data;
using Planner.Presenters;

namespace Planner.Views
{
    public partial class XmlEditorView : Form
    {
        #region Fields

        private readonly XmlEditorPresenter _presenter;
        private int _selectedRow;
        private string _path;

        #endregion

        #region Constructor(s)

        public XmlEditorView()
        {
            InitializeComponent();
            _presenter = new XmlEditorPresenter();
            Init();
        }

        #endregion

        #region Methods

        private void Init()
        {
            using (var dialog = new OpenFileDialog()
            {
                Filter = @"XML Files | *.xml",
                DefaultExt = ".xml"
            })
            {
                if (dialog.ShowDialog() != DialogResult.OK)
                    return;

                _presenter.DeserializeXmlCmd(_path = dialog.FileName);
            }

            classesGrid.DataSource = _presenter.Classes;
            classesGrid.Update();
            classesGrid.Refresh();
        }

        private void generateXmlButton_Click(object sender, EventArgs e)
        {
            _presenter.GenerateXmlCmd(_path);
            MessageBox.Show(@"Zapisano do pliku");
        }

        private void addToListButton_Click(object sender, EventArgs e)
        {
            //TODO: wrong data input
            if (string.IsNullOrEmpty(startHourText.Text)
                || string.IsNullOrEmpty(startMinutesText.Text)
                || string.IsNullOrEmpty(dayCombo.Text))
            {
                MessageBox.Show(@"Niepoprawnie uzupełnione dane");
                return;
            }

            var obj = new ClassObj
            {
                GroupCode = groupCodeText.Text,
                CourseCode = courseCodeText.Text,
                CourseName = courseNameText.Text,
                TeacherName = teacherNameText.Text,
                RoomNumber = roomText.Text,
                Day = DayToIntConverter.ConvertStringToDay(dayCombo.Text),
                StartHour = startHourText.Text + ":" + startMinutesText.Text,
                EndHour = endHourText.Text + ":" + endMinutesText.Text,
                WhichWeek = whichWeekCombo.Text
            };
            _presenter.AddToListCmd(obj);

            RefreshGrid();
            ClearView();
        }


        private void classesGrid_SelectionChanged(object sender, EventArgs e)
        {
            _selectedRow = classesGrid.CurrentCell.RowIndex;
        }

        private void deleteItemButton_Click(object sender, EventArgs e)
        {
            var itemToDelete = classesGrid.Rows[_selectedRow].Cells[0].Value as string;
            _presenter.DeleteItemCmd(itemToDelete);
            RefreshGrid();
        }

        private void editRecordButton_Click(object sender, EventArgs e)
        {
            var itemToEdit = classesGrid.Rows[_selectedRow].Cells[0].Value as string;
            var data = _presenter.GetData(itemToEdit);
            SetTextBoxs(data);
            RefreshGrid();
        }

        private void SetTextBoxs(ClassObj obj)
        {
            groupCodeText.Text = obj.GroupCode;
            courseCodeText.Text = obj.CourseCode;
            courseNameText.Text = obj.CourseName;
            dayCombo.Text = DayToIntConverter.ConvertDayToString(obj.Day);
            teacherNameText.Text = obj.TeacherName;
            roomText.Text = obj.RoomNumber;
            startHourText.Text = obj.StartHour.Substring(0, obj.StartHour.IndexOf(":", StringComparison.Ordinal));
            startMinutesText.Text = obj.StartHour.Substring(obj.StartHour.IndexOf(":", StringComparison.Ordinal) + 1);
            endHourText.Text = obj.EndHour.Substring(0, obj.EndHour.IndexOf(":", StringComparison.Ordinal));
            endMinutesText.Text = obj.EndHour.Substring(obj.EndHour.IndexOf(":", StringComparison.Ordinal) + 1);
            whichWeekCombo.Text = obj.WhichWeek;
        }


        private void ClearView()
        {
            groupCodeText.Text = string.Empty;
            courseCodeText.Text = string.Empty;
            courseNameText.Text = string.Empty;
            teacherNameText.Text = string.Empty;
            roomText.Text = string.Empty;
            dayCombo.Text = string.Empty;
            startHourText.Text = string.Empty;
            startMinutesText.Text = string.Empty;
            endHourText.Text = string.Empty;
            endMinutesText.Text = string.Empty;
            whichWeekCombo.Text = string.Empty;
        }

        private void RefreshGrid()
        {
            classesGrid.DataSource = null;
            classesGrid.DataSource = _presenter.Classes;
            classesGrid.Update();
            classesGrid.Refresh();
        }

        #endregion
    }
}