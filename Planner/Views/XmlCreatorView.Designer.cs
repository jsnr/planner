﻿namespace Planner.Views
{
    partial class XmlGeneratorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.courseCodeText = new System.Windows.Forms.TextBox();
            this.groupCodeText = new System.Windows.Forms.TextBox();
            this.groupCodeLabel = new System.Windows.Forms.Label();
            this.courseCodeLabel = new System.Windows.Forms.Label();
            this.teacherNameLabel = new System.Windows.Forms.Label();
            this.teacherNameText = new System.Windows.Forms.TextBox();
            this.addToListButton = new System.Windows.Forms.Button();
            this.generateXmlButton = new System.Windows.Forms.Button();
            this.courseNameText = new System.Windows.Forms.TextBox();
            this.roomText = new System.Windows.Forms.TextBox();
            this.courseNameLabel = new System.Windows.Forms.Label();
            this.roomLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.startHourLabel = new System.Windows.Forms.Label();
            this.classesGrid = new System.Windows.Forms.DataGridView();
            this.deleteItemButton = new System.Windows.Forms.Button();
            this.dayCombo = new System.Windows.Forms.ComboBox();
            this.startHourText = new System.Windows.Forms.ComboBox();
            this.startMinutesText = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.endMinutesText = new System.Windows.Forms.ComboBox();
            this.endHourText = new System.Windows.Forms.ComboBox();
            this.endHourLabel = new System.Windows.Forms.Label();
            this.whichWeekCombo = new System.Windows.Forms.ComboBox();
            this.whichWeek = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.classesGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // courseCodeText
            // 
            this.courseCodeText.Location = new System.Drawing.Point(125, 41);
            this.courseCodeText.Name = "courseCodeText";
            this.courseCodeText.Size = new System.Drawing.Size(136, 20);
            this.courseCodeText.TabIndex = 1;
            // 
            // groupCodeText
            // 
            this.groupCodeText.Location = new System.Drawing.Point(125, 15);
            this.groupCodeText.Name = "groupCodeText";
            this.groupCodeText.Size = new System.Drawing.Size(136, 20);
            this.groupCodeText.TabIndex = 0;
            // 
            // groupCodeLabel
            // 
            this.groupCodeLabel.AutoSize = true;
            this.groupCodeLabel.Location = new System.Drawing.Point(28, 18);
            this.groupCodeLabel.Name = "groupCodeLabel";
            this.groupCodeLabel.Size = new System.Drawing.Size(55, 13);
            this.groupCodeLabel.TabIndex = 2;
            this.groupCodeLabel.Text = "Kod grupy";
            // 
            // courseCodeLabel
            // 
            this.courseCodeLabel.AutoSize = true;
            this.courseCodeLabel.Location = new System.Drawing.Point(28, 44);
            this.courseCodeLabel.Name = "courseCodeLabel";
            this.courseCodeLabel.Size = new System.Drawing.Size(55, 13);
            this.courseCodeLabel.TabIndex = 3;
            this.courseCodeLabel.Text = "Kod kursu";
            // 
            // teacherNameLabel
            // 
            this.teacherNameLabel.AutoSize = true;
            this.teacherNameLabel.Location = new System.Drawing.Point(28, 102);
            this.teacherNameLabel.Name = "teacherNameLabel";
            this.teacherNameLabel.Size = new System.Drawing.Size(65, 13);
            this.teacherNameLabel.TabIndex = 4;
            this.teacherNameLabel.Text = "Prowadzacy";
            // 
            // teacherNameText
            // 
            this.teacherNameText.Location = new System.Drawing.Point(125, 99);
            this.teacherNameText.Name = "teacherNameText";
            this.teacherNameText.Size = new System.Drawing.Size(136, 20);
            this.teacherNameText.TabIndex = 3;
            // 
            // addToListButton
            // 
            this.addToListButton.Location = new System.Drawing.Point(59, 258);
            this.addToListButton.Name = "addToListButton";
            this.addToListButton.Size = new System.Drawing.Size(75, 23);
            this.addToListButton.TabIndex = 11;
            this.addToListButton.Text = "Dodaj";
            this.addToListButton.UseVisualStyleBackColor = true;
            this.addToListButton.Click += new System.EventHandler(this.addToListButton_Click);
            // 
            // generateXmlButton
            // 
            this.generateXmlButton.Location = new System.Drawing.Point(140, 258);
            this.generateXmlButton.Name = "generateXmlButton";
            this.generateXmlButton.Size = new System.Drawing.Size(75, 23);
            this.generateXmlButton.TabIndex = 12;
            this.generateXmlButton.Text = "XML";
            this.generateXmlButton.UseVisualStyleBackColor = true;
            this.generateXmlButton.Click += new System.EventHandler(this.generateXmlButton_Click);
            // 
            // courseNameText
            // 
            this.courseNameText.Location = new System.Drawing.Point(125, 70);
            this.courseNameText.Name = "courseNameText";
            this.courseNameText.Size = new System.Drawing.Size(136, 20);
            this.courseNameText.TabIndex = 2;
            // 
            // roomText
            // 
            this.roomText.Location = new System.Drawing.Point(125, 122);
            this.roomText.Name = "roomText";
            this.roomText.Size = new System.Drawing.Size(136, 20);
            this.roomText.TabIndex = 4;
            // 
            // courseNameLabel
            // 
            this.courseNameLabel.AutoSize = true;
            this.courseNameLabel.Location = new System.Drawing.Point(28, 73);
            this.courseNameLabel.Name = "courseNameLabel";
            this.courseNameLabel.Size = new System.Drawing.Size(69, 13);
            this.courseNameLabel.TabIndex = 11;
            this.courseNameLabel.Text = "Nazwa kursu";
            // 
            // roomLabel
            // 
            this.roomLabel.AutoSize = true;
            this.roomLabel.Location = new System.Drawing.Point(28, 125);
            this.roomLabel.Name = "roomLabel";
            this.roomLabel.Size = new System.Drawing.Size(28, 13);
            this.roomLabel.TabIndex = 12;
            this.roomLabel.Text = "Sala";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Dzien";
            // 
            // startHourLabel
            // 
            this.startHourLabel.AutoSize = true;
            this.startHourLabel.Location = new System.Drawing.Point(28, 177);
            this.startHourLabel.Name = "startHourLabel";
            this.startHourLabel.Size = new System.Drawing.Size(106, 13);
            this.startHourLabel.TabIndex = 14;
            this.startHourLabel.Text = "Godzina rozpoczecia";
            // 
            // classesGrid
            // 
            this.classesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.classesGrid.Location = new System.Drawing.Point(292, 15);
            this.classesGrid.Name = "classesGrid";
            this.classesGrid.Size = new System.Drawing.Size(436, 237);
            this.classesGrid.TabIndex = 16;
            this.classesGrid.SelectionChanged += new System.EventHandler(this.classesGrid_SelectionChanged);
            // 
            // deleteItemButton
            // 
            this.deleteItemButton.Location = new System.Drawing.Point(613, 258);
            this.deleteItemButton.Name = "deleteItemButton";
            this.deleteItemButton.Size = new System.Drawing.Size(115, 23);
            this.deleteItemButton.TabIndex = 17;
            this.deleteItemButton.Text = "Usuń zaznaczony";
            this.deleteItemButton.UseVisualStyleBackColor = true;
            this.deleteItemButton.Click += new System.EventHandler(this.deleteItemButton_Click);
            // 
            // dayCombo
            // 
            this.dayCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dayCombo.FormattingEnabled = true;
            this.dayCombo.Items.AddRange(new object[] {
            "Poniedziałek",
            "Wtorek",
            "Środa",
            "Czwartek",
            "Piątek",
            "Sobota",
            "Niedziela"});
            this.dayCombo.Location = new System.Drawing.Point(125, 148);
            this.dayCombo.Name = "dayCombo";
            this.dayCombo.Size = new System.Drawing.Size(136, 21);
            this.dayCombo.TabIndex = 5;
            // 
            // startHourText
            // 
            this.startHourText.FormattingEnabled = true;
            this.startHourText.Items.AddRange(new object[] {
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21"});
            this.startHourText.Location = new System.Drawing.Point(153, 175);
            this.startHourText.Name = "startHourText";
            this.startHourText.Size = new System.Drawing.Size(45, 21);
            this.startHourText.TabIndex = 6;
            // 
            // startMinutesText
            // 
            this.startMinutesText.FormattingEnabled = true;
            this.startMinutesText.Items.AddRange(new object[] {
            "00",
            "15",
            "30",
            "45"});
            this.startMinutesText.Location = new System.Drawing.Point(220, 175);
            this.startMinutesText.Name = "startMinutesText";
            this.startMinutesText.Size = new System.Drawing.Size(41, 21);
            this.startMinutesText.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(204, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(204, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = ":";
            // 
            // endMinutesText
            // 
            this.endMinutesText.FormattingEnabled = true;
            this.endMinutesText.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
            this.endMinutesText.Location = new System.Drawing.Point(220, 204);
            this.endMinutesText.Name = "endMinutesText";
            this.endMinutesText.Size = new System.Drawing.Size(41, 21);
            this.endMinutesText.TabIndex = 9;
            // 
            // endHourText
            // 
            this.endHourText.FormattingEnabled = true;
            this.endHourText.Items.AddRange(new object[] {
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21"});
            this.endHourText.Location = new System.Drawing.Point(153, 204);
            this.endHourText.Name = "endHourText";
            this.endHourText.Size = new System.Drawing.Size(45, 21);
            this.endHourText.TabIndex = 8;
            // 
            // endHourLabel
            // 
            this.endHourLabel.AutoSize = true;
            this.endHourLabel.Location = new System.Drawing.Point(28, 206);
            this.endHourLabel.Name = "endHourLabel";
            this.endHourLabel.Size = new System.Drawing.Size(109, 13);
            this.endHourLabel.TabIndex = 21;
            this.endHourLabel.Text = "Godzina zakończenia";
            // 
            // whichWeekCombo
            // 
            this.whichWeekCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.whichWeekCombo.FormattingEnabled = true;
            this.whichWeekCombo.Items.AddRange(new object[] {
            "Co tydzien",
            "Parzysty",
            "Nieparzysty"});
            this.whichWeekCombo.Location = new System.Drawing.Point(153, 231);
            this.whichWeekCombo.Name = "whichWeekCombo";
            this.whichWeekCombo.Size = new System.Drawing.Size(108, 21);
            this.whichWeekCombo.TabIndex = 10;
            // 
            // whichWeek
            // 
            this.whichWeek.AutoSize = true;
            this.whichWeek.Location = new System.Drawing.Point(28, 234);
            this.whichWeek.Name = "whichWeek";
            this.whichWeek.Size = new System.Drawing.Size(44, 13);
            this.whichWeek.TabIndex = 26;
            this.whichWeek.Text = "Tydzien";
            // 
            // XmlGeneratorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 289);
            this.Controls.Add(this.whichWeek);
            this.Controls.Add(this.whichWeekCombo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.endMinutesText);
            this.Controls.Add(this.endHourText);
            this.Controls.Add(this.endHourLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.startMinutesText);
            this.Controls.Add(this.startHourText);
            this.Controls.Add(this.dayCombo);
            this.Controls.Add(this.deleteItemButton);
            this.Controls.Add(this.classesGrid);
            this.Controls.Add(this.startHourLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.roomLabel);
            this.Controls.Add(this.courseNameLabel);
            this.Controls.Add(this.roomText);
            this.Controls.Add(this.courseNameText);
            this.Controls.Add(this.generateXmlButton);
            this.Controls.Add(this.addToListButton);
            this.Controls.Add(this.teacherNameText);
            this.Controls.Add(this.teacherNameLabel);
            this.Controls.Add(this.courseCodeLabel);
            this.Controls.Add(this.groupCodeLabel);
            this.Controls.Add(this.groupCodeText);
            this.Controls.Add(this.courseCodeText);
            this.Name = "XmlGeneratorView";
            this.Text = "XmlGeneratorView";
            ((System.ComponentModel.ISupportInitialize)(this.classesGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox courseCodeText;
        private System.Windows.Forms.TextBox groupCodeText;
        private System.Windows.Forms.Label groupCodeLabel;
        private System.Windows.Forms.Label courseCodeLabel;
        private System.Windows.Forms.Label teacherNameLabel;
        private System.Windows.Forms.TextBox teacherNameText;
        private System.Windows.Forms.Button addToListButton;
        private System.Windows.Forms.Button generateXmlButton;
        private System.Windows.Forms.TextBox courseNameText;
        private System.Windows.Forms.TextBox roomText;
        private System.Windows.Forms.Label courseNameLabel;
        private System.Windows.Forms.Label roomLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label startHourLabel;
        private System.Windows.Forms.DataGridView classesGrid;
        private System.Windows.Forms.Button deleteItemButton;
        private System.Windows.Forms.ComboBox dayCombo;
        private System.Windows.Forms.ComboBox startHourText;
        private System.Windows.Forms.ComboBox startMinutesText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox endMinutesText;
        private System.Windows.Forms.ComboBox endHourText;
        private System.Windows.Forms.Label endHourLabel;
        private System.Windows.Forms.ComboBox whichWeekCombo;
        private System.Windows.Forms.Label whichWeek;
    }
}

