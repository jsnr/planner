﻿namespace Planner.Views
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.deserializeXmlBut = new System.Windows.Forms.Button();
            this.generateBut = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.daneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generujXmlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujXmlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ustawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trybyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scenarioCombo = new System.Windows.Forms.ComboBox();
            this.scenarioLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.saveImageButton = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.saveTextToFile = new System.Windows.Forms.Button();
            this.coursesLabel = new System.Windows.Forms.Label();
            this.CoursesToSignText = new System.Windows.Forms.RichTextBox();
            this.pathText = new System.Windows.Forms.TextBox();
            this.XmlGroup = new System.Windows.Forms.GroupBox();
            this.generatorGroup = new System.Windows.Forms.GroupBox();
            this.CancelButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.XmlGroup.SuspendLayout();
            this.generatorGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // deserializeXmlBut
            // 
            this.deserializeXmlBut.Location = new System.Drawing.Point(377, 17);
            this.deserializeXmlBut.Name = "deserializeXmlBut";
            this.deserializeXmlBut.Size = new System.Drawing.Size(75, 23);
            this.deserializeXmlBut.TabIndex = 0;
            this.deserializeXmlBut.Text = "Wybierz";
            this.deserializeXmlBut.UseVisualStyleBackColor = true;
            this.deserializeXmlBut.Click += new System.EventHandler(this.deserializeXmlBut_Click);
            // 
            // generateBut
            // 
            this.generateBut.Location = new System.Drawing.Point(184, 56);
            this.generateBut.Name = "generateBut";
            this.generateBut.Size = new System.Drawing.Size(75, 23);
            this.generateBut.TabIndex = 1;
            this.generateBut.Text = "Generuj plan";
            this.generateBut.UseVisualStyleBackColor = true;
            this.generateBut.Click += new System.EventHandler(this.generateBut_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.daneToolStripMenuItem,
            this.ustawieniaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(484, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // daneToolStripMenuItem
            // 
            this.daneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generujXmlToolStripMenuItem,
            this.edytujXmlToolStripMenuItem});
            this.daneToolStripMenuItem.Name = "daneToolStripMenuItem";
            this.daneToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.daneToolStripMenuItem.Text = "Dane";
            // 
            // generujXmlToolStripMenuItem
            // 
            this.generujXmlToolStripMenuItem.Name = "generujXmlToolStripMenuItem";
            this.generujXmlToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.generujXmlToolStripMenuItem.Text = "Generuj Xml";
            this.generujXmlToolStripMenuItem.Click += new System.EventHandler(this.generujXmlToolStripMenuItem_Click);
            // 
            // edytujXmlToolStripMenuItem
            // 
            this.edytujXmlToolStripMenuItem.Name = "edytujXmlToolStripMenuItem";
            this.edytujXmlToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.edytujXmlToolStripMenuItem.Text = "Edytuj Xml";
            this.edytujXmlToolStripMenuItem.Click += new System.EventHandler(this.edytujXmlToolStripMenuItem_Click);
            // 
            // ustawieniaToolStripMenuItem
            // 
            this.ustawieniaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trybyToolStripMenuItem});
            this.ustawieniaToolStripMenuItem.Name = "ustawieniaToolStripMenuItem";
            this.ustawieniaToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.ustawieniaToolStripMenuItem.Text = "Ustawienia";
            // 
            // trybyToolStripMenuItem
            // 
            this.trybyToolStripMenuItem.Name = "trybyToolStripMenuItem";
            this.trybyToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.trybyToolStripMenuItem.Text = "Ustawienia aplikacji";
            this.trybyToolStripMenuItem.Click += new System.EventHandler(this.trybyToolStripMenuItem_Click);
            // 
            // scenarioCombo
            // 
            this.scenarioCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scenarioCombo.FormattingEnabled = true;
            this.scenarioCombo.Items.AddRange(new object[] {
            "Jak najwczesniej",
            "Jak najpozniej",
            "Jak najwiecej dni wolnych",
            "Jak najmniej okienek"});
            this.scenarioCombo.Location = new System.Drawing.Point(97, 29);
            this.scenarioCombo.Name = "scenarioCombo";
            this.scenarioCombo.Size = new System.Drawing.Size(273, 21);
            this.scenarioCombo.TabIndex = 3;
            // 
            // scenarioLabel
            // 
            this.scenarioLabel.AutoSize = true;
            this.scenarioLabel.Location = new System.Drawing.Point(6, 32);
            this.scenarioLabel.Name = "scenarioLabel";
            this.scenarioLabel.Size = new System.Drawing.Size(85, 13);
            this.scenarioLabel.TabIndex = 4;
            this.scenarioLabel.Text = "Wybierz priorytet";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Planner.Properties.Resources.awd_tlo;
            this.pictureBox1.Location = new System.Drawing.Point(6, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(441, 223);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Location = new System.Drawing.Point(12, 214);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(458, 287);
            this.tabControl.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage1.Controls.Add(this.saveImageButton);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(450, 261);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Proponowany plan";
            // 
            // saveImageButton
            // 
            this.saveImageButton.Location = new System.Drawing.Point(369, 232);
            this.saveImageButton.Name = "saveImageButton";
            this.saveImageButton.Size = new System.Drawing.Size(75, 23);
            this.saveImageButton.TabIndex = 6;
            this.saveImageButton.Text = "Zapisz";
            this.saveImageButton.UseVisualStyleBackColor = true;
            this.saveImageButton.Visible = false;
            this.saveImageButton.Click += new System.EventHandler(this.saveImageButton_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage2.Controls.Add(this.saveTextToFile);
            this.tabPage2.Controls.Add(this.coursesLabel);
            this.tabPage2.Controls.Add(this.CoursesToSignText);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(450, 261);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Kursy do zapisu";
            // 
            // saveTextToFile
            // 
            this.saveTextToFile.Location = new System.Drawing.Point(369, 210);
            this.saveTextToFile.Name = "saveTextToFile";
            this.saveTextToFile.Size = new System.Drawing.Size(75, 23);
            this.saveTextToFile.TabIndex = 3;
            this.saveTextToFile.Text = "Zapisz";
            this.saveTextToFile.UseVisualStyleBackColor = true;
            this.saveTextToFile.Visible = false;
            this.saveTextToFile.Click += new System.EventHandler(this.saveTextToFile_Click);
            // 
            // coursesLabel
            // 
            this.coursesLabel.AutoSize = true;
            this.coursesLabel.Location = new System.Drawing.Point(133, 3);
            this.coursesLabel.Name = "coursesLabel";
            this.coursesLabel.Size = new System.Drawing.Size(166, 13);
            this.coursesLabel.TabIndex = 2;
            this.coursesLabel.Text = "Kursy na które należy się zapisać:";
            // 
            // CoursesToSignText
            // 
            this.CoursesToSignText.Location = new System.Drawing.Point(136, 26);
            this.CoursesToSignText.Name = "CoursesToSignText";
            this.CoursesToSignText.Size = new System.Drawing.Size(161, 207);
            this.CoursesToSignText.TabIndex = 0;
            this.CoursesToSignText.Text = "";
            // 
            // pathText
            // 
            this.pathText.Location = new System.Drawing.Point(6, 19);
            this.pathText.Name = "pathText";
            this.pathText.Size = new System.Drawing.Size(365, 20);
            this.pathText.TabIndex = 7;
            // 
            // XmlGroup
            // 
            this.XmlGroup.Controls.Add(this.pathText);
            this.XmlGroup.Controls.Add(this.deserializeXmlBut);
            this.XmlGroup.Location = new System.Drawing.Point(12, 38);
            this.XmlGroup.Name = "XmlGroup";
            this.XmlGroup.Size = new System.Drawing.Size(458, 51);
            this.XmlGroup.TabIndex = 8;
            this.XmlGroup.TabStop = false;
            this.XmlGroup.Text = "Xml";
            // 
            // generatorGroup
            // 
            this.generatorGroup.Controls.Add(this.CancelButton);
            this.generatorGroup.Controls.Add(this.scenarioCombo);
            this.generatorGroup.Controls.Add(this.generateBut);
            this.generatorGroup.Controls.Add(this.scenarioLabel);
            this.generatorGroup.Location = new System.Drawing.Point(13, 108);
            this.generatorGroup.Name = "generatorGroup";
            this.generatorGroup.Size = new System.Drawing.Size(457, 87);
            this.generatorGroup.TabIndex = 9;
            this.generatorGroup.TabStop = false;
            this.generatorGroup.Text = "Generator";
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(372, 56);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 5;
            this.CancelButton.Text = "Anuluj";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(484, 514);
            this.Controls.Add(this.generatorGroup);
            this.Controls.Add(this.XmlGroup);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainView";
            this.Text = "Planner";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.XmlGroup.ResumeLayout(false);
            this.XmlGroup.PerformLayout();
            this.generatorGroup.ResumeLayout(false);
            this.generatorGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button deserializeXmlBut;
        private System.Windows.Forms.Button generateBut;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem daneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generujXmlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujXmlToolStripMenuItem;
        private System.Windows.Forms.ComboBox scenarioCombo;
        private System.Windows.Forms.Label scenarioLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox pathText;
        private System.Windows.Forms.GroupBox XmlGroup;
        private System.Windows.Forms.GroupBox generatorGroup;
        private System.Windows.Forms.Label coursesLabel;
        private System.Windows.Forms.RichTextBox CoursesToSignText;
        private System.Windows.Forms.Button saveTextToFile;
        private System.Windows.Forms.ToolStripMenuItem ustawieniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trybyToolStripMenuItem;
        private System.Windows.Forms.Button saveImageButton;
        private System.Windows.Forms.Button CancelButton;
    }
}