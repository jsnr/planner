﻿namespace Planner.Views
{
    partial class AppConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IsThreadingEnabled = new System.Windows.Forms.CheckBox();
            this.isThreadingEnabledLabel = new System.Windows.Forms.Label();
            this.Group1 = new System.Windows.Forms.GroupBox();
            this.pathToSave = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.saveConfigButton = new System.Windows.Forms.Button();
            this.Group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IsThreadingEnabled
            // 
            this.IsThreadingEnabled.AutoSize = true;
            this.IsThreadingEnabled.Location = new System.Drawing.Point(174, 24);
            this.IsThreadingEnabled.Name = "IsThreadingEnabled";
            this.IsThreadingEnabled.Size = new System.Drawing.Size(15, 14);
            this.IsThreadingEnabled.TabIndex = 0;
            this.IsThreadingEnabled.UseVisualStyleBackColor = true;
            // 
            // isThreadingEnabledLabel
            // 
            this.isThreadingEnabledLabel.AutoSize = true;
            this.isThreadingEnabledLabel.Location = new System.Drawing.Point(9, 24);
            this.isThreadingEnabledLabel.Name = "isThreadingEnabledLabel";
            this.isThreadingEnabledLabel.Size = new System.Drawing.Size(97, 13);
            this.isThreadingEnabledLabel.TabIndex = 1;
            this.isThreadingEnabledLabel.Text = "Tryb wielowątkowy";
            // 
            // Group1
            // 
            this.Group1.Controls.Add(this.pathToSave);
            this.Group1.Controls.Add(this.label1);
            this.Group1.Controls.Add(this.isThreadingEnabledLabel);
            this.Group1.Controls.Add(this.IsThreadingEnabled);
            this.Group1.Location = new System.Drawing.Point(12, 12);
            this.Group1.Name = "Group1";
            this.Group1.Size = new System.Drawing.Size(567, 90);
            this.Group1.TabIndex = 2;
            this.Group1.TabStop = false;
            this.Group1.Text = "Zarządzanie zasobami";
            // 
            // pathToSave
            // 
            this.pathToSave.Location = new System.Drawing.Point(174, 49);
            this.pathToSave.Name = "pathToSave";
            this.pathToSave.Size = new System.Drawing.Size(387, 20);
            this.pathToSave.TabIndex = 4;
            this.pathToSave.Text = "D:\\";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Domyślna ścieżka zapisu tekstu:";
            // 
            // saveConfigButton
            // 
            this.saveConfigButton.Location = new System.Drawing.Point(504, 108);
            this.saveConfigButton.Name = "saveConfigButton";
            this.saveConfigButton.Size = new System.Drawing.Size(75, 23);
            this.saveConfigButton.TabIndex = 3;
            this.saveConfigButton.Text = "Ok";
            this.saveConfigButton.UseVisualStyleBackColor = true;
            this.saveConfigButton.Click += new System.EventHandler(this.saveConfigButton_Click);
            // 
            // AppConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(595, 140);
            this.Controls.Add(this.saveConfigButton);
            this.Controls.Add(this.Group1);
            this.Name = "AppConfig";
            this.Text = "AppConfig";
            this.Group1.ResumeLayout(false);
            this.Group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox IsThreadingEnabled;
        private System.Windows.Forms.Label isThreadingEnabledLabel;
        private System.Windows.Forms.GroupBox Group1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pathToSave;
        private System.Windows.Forms.Button saveConfigButton;
    }
}