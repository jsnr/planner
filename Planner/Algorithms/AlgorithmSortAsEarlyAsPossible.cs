﻿using System.Collections.Generic;
using System.Linq;
using Planner.Data;

namespace Planner.Model
{
    internal class AlgorithmSortAsEarlyAsPossible : BaseSort, IAlgorithm
    {
        #region Constructor(s)

        public AlgorithmSortAsEarlyAsPossible(List<ClassObj> classes):
            base(classes)
        {

        }

        #endregion

        #region Methods

        public void Sort()
        {
            var firstElement = Courses.First();
            var latestStartHour = AllClasses.Where(c => c.CourseCode == firstElement.CourseCode).Min(h => h.StartHour);
            var itemToAdd =
                AllClasses.FirstOrDefault(c => c.CourseCode == firstElement.CourseCode && c.StartHour == latestStartHour);

            if (!IsClassCollidingWithOther(itemToAdd))
            {
                CreatedPlan.Add(itemToAdd);
            }
            else
            {
                var listOfCollidingItems = AllClasses.Where(c => { return itemToAdd != null && c.CourseCode == itemToAdd.CourseCode; }).ToList();
                do
                {
                    listOfCollidingItems.Remove(itemToAdd);
                    latestStartHour = listOfCollidingItems.Min(h => h.StartHour);
                    itemToAdd =
                        AllClasses.FirstOrDefault(
                            c => c.CourseCode == firstElement.CourseCode && c.StartHour == latestStartHour);
                } while (IsClassCollidingWithOther(itemToAdd) && listOfCollidingItems.Count != 0);

                CreatedPlan.Add(itemToAdd);
            }

            DeleteCourse(firstElement);

            if (Courses.Count != 0)
            {
                Sort();
            }

        }

        public List<ClassObj> GetSorted()
        {
            return CreatedPlan;
        }
        #endregion
    }
}