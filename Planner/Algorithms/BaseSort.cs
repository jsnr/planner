﻿using System;
using System.Collections.Generic;
using System.Linq;
using Planner.Converters;
using Planner.Data;

namespace Planner.Model
{
    internal abstract class BaseSort
    {
        #region Fields

        private List<ClassObj> _allClasses;
        private List<CoursesObj> _courses;
        private List<ClassObj> _createdPlan;

        #endregion

        #region Properties

        public List<ClassObj> AllClasses
        {
            get { return _allClasses; }
            protected set
            {
                if (value.Count == 0)
                    throw new ArgumentException(@"AllClasses cannot be an empty collection.");
                _allClasses = value;
            }
        }

        public List<CoursesObj> Courses
        {
            get { return _courses; }
            protected set
            {
                if (value.Count == 0)
                    throw new ArgumentException(@"Courses cannot be an empty collection.");
                _courses = value;
            }
        }

        public List<ClassObj> CreatedPlan
        {
            get { return _createdPlan; }
            protected set
            {
                if (value.Count == 0)
                    throw new ArgumentException(@"Created cannot be an empty collection.", nameof(value));
                _createdPlan = value;
            }
        }

        #endregion

        #region Constructor(s)

        public BaseSort(List<ClassObj> classes)
        {
            AllClasses = classes;
            CheckHowManyCourses();
            CreatedPlan = PutCoursesInPlanIfOnlyOneOccurence();
        }
#endregion

        #region Methods

        protected void CheckHowManyCourses()
        {
            var result = new List<CoursesObj>();
            foreach (var classObj in AllClasses)
            {
                if (!result.Exists(course => course.CourseCode == classObj.CourseCode))
                {
                    result.Add(new CoursesObj() {CourseCode = classObj.CourseCode, CourseOccurenceInPlan = 1});
                    continue;
                }

                var obj = result.FirstOrDefault(course => course.CourseCode == classObj.CourseCode);
                if (obj != null)
                    obj.CourseOccurenceInPlan += 1;
            }

            Courses = result.OrderByDescending(c => c.CourseOccurenceInPlan).ToList();
            Courses.Reverse();
        }

        protected List<ClassObj> PutCoursesInPlanIfOnlyOneOccurence()
        {
            var result = new List<ClassObj>();

            var coursesToDelete = new List<CoursesObj>();
            foreach (var coursesObj in Courses)
            {
                if (coursesObj.CourseOccurenceInPlan == 1)
                {
                    var classObj = AllClasses.FirstOrDefault(c => c.CourseCode == coursesObj.CourseCode);
                    result.Add(classObj);
                    coursesToDelete.Add(coursesObj);
                }
            }

            DeleteCourses(coursesToDelete);
            return result;
        }

        protected void DeleteCourse(CoursesObj courseToDelete)
        {
            Courses.Remove(courseToDelete);
        }

        protected void DeleteCourses(List<CoursesObj> coursesToDelete)
        {
            foreach (var coursesObj in coursesToDelete)
            {
                Courses.Remove(coursesObj);
            }
        }

        protected bool IsClassCollidingWithOther(ClassObj classObj)
        {
            var listStartHour = CreatedPlan.Where(d => d.Day == classObj.Day).ToList();
            listStartHour.Remove(classObj);

            foreach (var classes in listStartHour)
            {
                if (AreHoursOfClassesSame(classObj, classes))
                {
                    return true;
                }
            }
            return false;
        }

        protected bool AreHoursOfClassesSame(ClassObj classToCheck, ClassObj checkingClass)
        {
            var a = new HourToIntConverter(classToCheck.StartHour, classToCheck.EndHour);
            var b = new HourToIntConverter(checkingClass.StartHour, checkingClass.EndHour);

            if (classToCheck.WhichWeek == checkingClass.WhichWeek || checkingClass.WhichWeek == "Co tydzien")
            {
                if (a.StartHour <= b.StartHour && a.EndHour >= b.StartHour)
                    return true;
                if (a.StartHour <= b.EndHour && a.EndHour >= b.EndHour)
                    return true;
                if (a.StartHour >= b.StartHour && a.EndHour <= b.EndHour)
                    return true;
                if (a.StartHour <= b.StartHour && a.EndHour >= b.EndHour)
                    return true;
            }
            return false;
        }

        #endregion
    }
}