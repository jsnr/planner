﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Planner.Data;

namespace Planner.Presenters
{
    public class XmlEditorPresenter : XmlBasePresenter
    {
        public XmlEditorPresenter()
        {
            Classes = new List<ClassObj>();
        }

        internal ClassObj GetData(string itemToEdit)
        {
            return Classes.FirstOrDefault(s => s.GroupCode == itemToEdit);
        }

        internal void DeserializeXmlCmd(string path)
        {
            Classes = DeserializeXml(path);
        }

        private static List<ClassObj> DeserializeXml(string path)
        {
            Classes result;

            using (var reader = new StreamReader(path))
            {
                var deserializer = new XmlSerializer(typeof(Classes));
                result = (Classes)deserializer.Deserialize(reader);
            }

            return result.Class;
        }

        public override void AddToListCmd(ClassObj obj)
        {
            if (Classes.Exists(s => s.GroupCode == obj.GroupCode))
            {
                DeleteItemCmd(obj.GroupCode);
            }
            base.AddToListCmd(obj);
        }
    }
}
