﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using Planner.Common;
using Planner.Data;
using Planner.Exceptions;
using Planner.Generators;
using Planner.Model;
using Planner.Properties;
using Planner.Views;

namespace Planner.Presenters
{
    public class MainPresenter
    {
        #region  Fields

        private readonly MainView _mainView;

        #endregion

        #region Properties

        public List<ClassObj> ClassesList { get; private set; }

        #endregion

        #region Constructor(s)

        public MainPresenter(MainView mainView)
        {
            _mainView = mainView;
        }

        #endregion

        #region Methods

        public void DeserializeXmlCmd(string path)
        {
            ClassesList = DeserializeXml(path);
        }

        private static List<ClassObj> DeserializeXml(string path)
        {
            Classes result;

            using (var reader = new StreamReader(path))
            {
                var deserializer = new XmlSerializer(typeof(Classes));
                result = (Classes) deserializer.Deserialize(reader);
            }

            return result.Class;
        }
        
        public void CreatePlanCmd(SortingScenarios scenario, CancellationToken token)
        {
            //HACK: too lazy to put somewhere "else" 
            _mainView.Plan = Resources.awd_tlo;

            if (token.IsCancellationRequested)
            {
                _mainView.ShowMessage = "Canceled before even started!";
                _mainView.GenerateButVisibility = true;
                token.ThrowIfCancellationRequested();
            }
                
            var model = new SortingAlgorithm(ClassesList, scenario);

            foreach (var classObj in model.AllClasses)
            {
                if (token.IsCancellationRequested)
                {
                    _mainView.ShowMessage = "User abort!";
                    _mainView.GenerateButVisibility = true;
                    return;
                }
                CreateLazyViewFromList(classObj);
            }

            _mainView.GenerateButVisibility = false;
        }

        private void CreateLazyViewFromList(ClassObj classObj)
        {
            var tableGenerator = new TableGenerator(_mainView.Plan as Bitmap);
            tableGenerator.Insert(classObj.CourseName, classObj.StartHour, classObj.EndHour, classObj.Day,
                classObj.TeacherName, classObj.WhichWeek, GetColorForClassObj(classObj));
            _mainView.Plan = tableGenerator.GetCreatedObject() as Image;
            _mainView.CoursesToSign = classObj.GroupCode;
        }

        private static Color GetColorForClassObj(ClassObj classObj)
        {
            if (classObj.CourseCode.EndsWith("W"))
            {
                return Color.FromArgb(255, 26, 26);
            }
            else if (classObj.CourseCode.EndsWith("L"))
            {
                return Color.FromArgb(26, 26, 255);
            }
            else if (classObj.CourseCode.EndsWith("S"))
            {
                return Color.FromArgb(255, 255, 0);
            }
            else if (classObj.CourseCode.EndsWith("C"))
            {
                return Color.FromArgb(0, 204, 0);
            }
            else if (classObj.CourseCode.EndsWith("P"))
            {
                return Color.FromArgb(153, 0, 153);
            }
            throw new PlannerAlgorithmException("Cannot parse color");
        }

        #endregion

        public void SaveToFile(string coursesToSign)
        {
            File.WriteAllText(_mainView.AppConfigObj.PathToSave+ @"\ZajeciaDoZapisu.txt", coursesToSign);
        }

        public void SaveToFile(Image coursesToSign)
        {
            coursesToSign.Save(_mainView.AppConfigObj.PathToSave + @"\ProponowanyPlan.png", ImageFormat.Jpeg);
        }
    }
}