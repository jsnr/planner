﻿using System.Collections.Generic;
using Planner.Data;

namespace Planner.Presenters
{
    public class XmlCreatorPresenter : XmlBasePresenter
    {
        public XmlCreatorPresenter()
        {
            Classes = new List<ClassObj>();
        }

        public void GenerateXmlCmd(string path)
        {
            base.GenerateXmlCmd(path);
        }
    }
}
