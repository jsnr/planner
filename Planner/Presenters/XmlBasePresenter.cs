﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Planner.Data;

namespace Planner.Presenters
{
    public class XmlBasePresenter
    {
        #region Properties
        public List<ClassObj> Classes { get; set; }

        #endregion

        #region Methods

        public XmlBasePresenter()
        {
            Classes = new List<ClassObj>();
        }

        public virtual void AddToListCmd(ClassObj obj)
        {
            Classes.Add(obj);
        }

        public void GenerateXmlCmd(string path)
        {
            GenerateXml(path);
        }

        public void DeleteItemCmd(string itemGroupCode)
        {
            var itemToDelete = Classes.FirstOrDefault(i => i.GroupCode == itemGroupCode);
            Classes.Remove(itemToDelete);
        }

        private void GenerateXml(string path)
        {
            var classes = new Classes();
            foreach (var classObj in Classes)
            {
                classes.Class.Add(classObj);
            }

            var serializer = new XmlSerializer(typeof(Classes));
            using (var fs = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(fs, classes);
            }
        }
        #endregion
    }
}