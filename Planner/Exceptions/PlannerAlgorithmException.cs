﻿using System;
using System.Runtime.Serialization;

namespace Planner.Exceptions
{
    [Serializable]
    internal class PlannerAlgorithmException : Exception
    {
        public PlannerAlgorithmException()
        {
        }

        public PlannerAlgorithmException(string message) : base(message)
        {
        }

        public PlannerAlgorithmException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PlannerAlgorithmException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}