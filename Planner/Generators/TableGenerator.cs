﻿using System;
using System.Drawing;
using Planner.Converters;
using Planner.Exceptions;
using Planner.Properties;

namespace Planner.Generators
{
    class TableGenerator : IGenerator
    {
        #region Fields

        private readonly Bitmap _planBitmap = Resources.awd_tlo;

        #endregion

        #region Constructor(s)

        public TableGenerator()
        {
            
        }
        public TableGenerator(Bitmap imageToDrawOn)
        {
            _planBitmap = imageToDrawOn;
        }

        #endregion

        #region Methods

        public void Insert(string name, string startHour, string endHour)
        {
            throw new NotImplementedException();
        }

        public void Insert(string name, string startHour, string endHour, int day, string teacherName)
        {
            throw new NotImplementedException();
        }

        public void Insert(string name, string startHour, string endHour, int day, string teacherName, string week,
            Color color)
        {
            using (var graphics = Graphics.FromImage(_planBitmap))
            {
                using (var arialFont = new Font("Arial", 20))
                {
                    if (week == "Parzysty")
                    {
                        var rect = new Rectangle((int) ConvertDayToXPosition(day) + 200,
                            (int) ConvertHourToYPosition(startHour), 180, (int) GetHeight(startHour, endHour));
                        graphics.FillRectangle(new SolidBrush(color), rect);
                        rect.Inflate(-10, -10);
                        graphics.DrawString(name + "\n" + teacherName, arialFont, new SolidBrush(Color.Black), rect);
                    }
                    else if (week == "Nieparzysty")
                    {
                        var rect = new Rectangle((int) ConvertDayToXPosition(day),
                            (int) ConvertHourToYPosition(startHour), 180, (int) GetHeight(startHour, endHour));
                        graphics.FillRectangle(new SolidBrush(color), rect);
                        rect.Inflate(-10, -10);
                        graphics.DrawString(name + "\n" + teacherName, arialFont, new SolidBrush(Color.Black), rect);
                    }
                    else
                    {
                        graphics.FillRectangle(new SolidBrush(color), ConvertDayToXPosition(day),
                            ConvertHourToYPosition(startHour), 400, GetHeight(startHour, endHour));
                        graphics.DrawString(name, arialFont, new SolidBrush(Color.Black),
                            ConvertDayToXPosition(day) + 10, ConvertHourToYPosition(startHour) + 10);
                        graphics.DrawString(teacherName, arialFont, new SolidBrush(Color.Black),
                            ConvertDayToXPosition(day) + 10, ConvertHourToYPosition(startHour) + 40);
                    }
                }
            }
        }

        private float ConvertDayToXPosition(int day)
        {
            switch (day)
            {
                case 1:
                    return 160;
                case 2:
                    return 580;
                case 3:
                    return 1000;
                case 4:
                    return 1420;
                case 5:
                    return 1840;
            }
            throw new PlannerAlgorithmException("Convert day to pixel");
        }

        private float ConvertHourToYPosition(string startHour)
        {
            int hour, minutes;
            HourToIntConverter.ConvertToInt(startHour, out hour, out minutes);

            var position = (float) ((hour - 7) + ((double) minutes/60))*100 + 100;

            return position;
        }

        private float GetHeight(string startHour, string endHour)
        {
            int startH, startM, endH, endM;
            HourToIntConverter.ConvertToInt(startHour, out startH, out startM);
            HourToIntConverter.ConvertToInt(endHour, out endH, out endM);

            return ((endH - 6)*100 + (100*endM/60)) - ((startH - 6)*100 + (100*startM/60));
        }

        public object GetCreatedObject()
        {
            return _planBitmap;
        }

        #endregion
    }
}