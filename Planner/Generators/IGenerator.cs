﻿namespace Planner.Generators
{
    internal interface IGenerator
    {
        void Insert(string name, string startHour, string endHour);
        void Insert(string name, string startHour, string endHour, int day, string teacherName);
        object GetCreatedObject(); 
    }
}