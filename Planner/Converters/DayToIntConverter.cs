﻿using Planner.Exceptions;

namespace Planner.Converters
{
    public static class DayToIntConverter
    {
        #region Methods

        public static int ConvertStringToDay(string dayComboText)
        {
            switch (dayComboText)
            {
                case "Poniedzialek":
                    return 1;
                case "Wtorek":
                    return 2;
                case "Środa":
                    return 3;
                case "Czwartek":
                    return 4;
                case "Piątek":
                    return 5;
                case "Sobota":
                    return 6;
                case "Niedziela":
                    return 7;
            }
            throw new ConverterException("Nieudana konwersja");
        }

        public static string ConvertDayToString(int objDay)
        {
            switch (objDay)
            {
                case 1:
                    return "Poniedzialek";
                case 2:
                    return "Wtorek";
                case 3:
                    return "Sroda";
                case 4:
                    return "Czwartek";
                case 5:
                    return "Piatek";
                case 6:
                    return "Sobota";
                case 7:
                    return "Niedziela";
            }
            throw new ConverterException("Nieudana konwersja");
        }

        #endregion
    }
}