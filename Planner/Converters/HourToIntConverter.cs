﻿namespace Planner.Converters
{
    public class HourToIntConverter
    {
        #region Methods

        public static int ConvertToInt(string hourToConvert)
        {
            var hours = hourToConvert.Split(':');
            var result = hours[0] + hours[1];
            return int.Parse(result);
        }

        public static void ConvertToInt(string hourToConvert, out int hour, out int minutes)
        {
            var splittedHour = hourToConvert.Split(':');
            hour = int.Parse(splittedHour[0]);
            minutes = int.Parse(splittedHour[1]);
        }

        public static int StringHourToInt(string hourToConvert, int maxBreak)
        {
            var rawHours = hourToConvert.Split(':');
            var hours = new[]{int.Parse(rawHours[0]), int.Parse(rawHours[1]) };

            if (hours[1] + maxBreak >= 60)
            {
                hours[0] += 1;
                hours[1] -= maxBreak;
            }
            else
            {
                hours[1] += maxBreak;
            }

            return hours[0] * 100 + hours[1];
        }

        #endregion
    }
}
