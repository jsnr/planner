﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Planner.Common;
using Planner.Data;
using Planner.Exceptions;

namespace Planner.Model
{
    internal class SortingAlgorithm
    {
        #region Properties

        public List<ClassObj> AllClasses { get; private set; }

        #endregion

        #region Constructor(s)

        public SortingAlgorithm(List<ClassObj> allClasses, SortingScenarios scenario)
        {
            if (allClasses == null)
                throw new ArgumentNullException(nameof(allClasses));
            if (!Enum.IsDefined(typeof(SortingScenarios), scenario))
                throw new InvalidEnumArgumentException(nameof(scenario), (int) scenario, typeof(SortingScenarios));

            AllClasses = allClasses;
            AllClasses = CreatePlan(scenario);
        }

        #endregion

        #region Methods

        private List<ClassObj> CreatePlan(SortingScenarios scenarios)
        {
            IAlgorithm sortBuilder = null;
            switch (scenarios)
            {
                case SortingScenarios.AsEarlyAsPossible:
                {
                    sortBuilder = new AlgorithmSortAsEarlyAsPossible(AllClasses);
                    break;
                }
                case SortingScenarios.AsLateAsPossible:
                {
                    sortBuilder = new AlgorithmSortAsLateAsPossible(AllClasses);
                    break;
                }

                case SortingScenarios.ManyDaysFree:
                {
                    throw new NotImplementedException();
                    break;
                }

                case SortingScenarios.NoBreaks:
                {
                    sortBuilder = new AlgorithmSortNoBreaks(AllClasses);
                    break;
                }
            }

            sortBuilder.Sort();
            var result = sortBuilder.GetSorted();

            if (result != null)
            {
                return result.OrderByDescending(c => c.Day).ToList();
            }
            else
            {
                throw new PlannerAlgorithmException("Sorting algorithm");
            }
        }

        #endregion
    }
}