﻿using System;
using System.Collections.Generic;
using System.Linq;
using Planner.Data;
using Planner.Converters;

namespace Planner.Model
{
    class AlgorithmSortNoBreaks : BaseSort, IAlgorithm
    {
        #region Constructor(s)

        public AlgorithmSortNoBreaks(List<ClassObj> classes) :
            base(classes)
        {
        }

        #endregion

        #region Methods
        public void Sort()
        {
            var coursesDetail = new List<ClassObj>(AllClasses);
            DeleteCourses(CreatedPlan, coursesDetail);

            int iter = 0, max = 0;
            var dayWithMaxAmountOfClasses = 99;
            var whichWeekIsThis = string.Empty;
            var week = new List<TheDay>
            {
                new TheDay {Id = 1, WasVisitedOnNWeek = false, WasVisitedOnPWeek = false },
                new TheDay {Id = 2, WasVisitedOnNWeek = false, WasVisitedOnPWeek = false },
                new TheDay {Id = 3, WasVisitedOnNWeek = false, WasVisitedOnPWeek = false },
                new TheDay {Id = 4, WasVisitedOnNWeek = false, WasVisitedOnPWeek = false },
                new TheDay {Id = 5, WasVisitedOnNWeek = false, WasVisitedOnPWeek = false },
            };

            do
            {
                foreach (var day in week)
                {
                    var howManyClassesN = CreatedPlan.Count(a => a.Day == day.Id
                                                && (a.WhichWeek == "Co tydzien" || a.WhichWeek == "Nieparzysty"));
                    var howManyClassesP = CreatedPlan.Count(a => a.Day == day.Id
                                                                 &&
                                                                 (a.WhichWeek == "Co tydzien" ||
                                                                  a.WhichWeek == "Parzysty"));

                    if (howManyClassesN >= max && day.WasVisitedOnNWeek == false)
                    {
                        max = howManyClassesN;
                        dayWithMaxAmountOfClasses = day.Id;
                        whichWeekIsThis = "Nieparzysty";
                    }

                    if (howManyClassesP >= max && day.WasVisitedOnPWeek == false)
                    {
                        max = howManyClassesP;
                        dayWithMaxAmountOfClasses = day.Id;
                        whichWeekIsThis = "Parzysty";
                    }
                }

                max = 0;
                iter++;

                var listOfClassesToAdd = coursesDetail.Where(a => a.Day == dayWithMaxAmountOfClasses
                                       && (a.WhichWeek == whichWeekIsThis || a.WhichWeek == "Co tydzien")).ToList();

                foreach (var classToAdd in listOfClassesToAdd)
                {
                    var ifWasAdded = CreatedPlan.Exists(a => a.Day == dayWithMaxAmountOfClasses
                                        && a.CourseCode == classToAdd.CourseCode);

                    if (!IsClassCollidingWithOther(classToAdd) && !ifWasAdded)
                    {
                        CreatedPlan.Add(classToAdd);
                        var coursesToDelete = AllClasses.Where(a => a.CourseCode == classToAdd.CourseCode).ToList();
                        DeleteCourses(coursesToDelete, coursesDetail);
                    }
                }

                if (whichWeekIsThis == "Parzysty")
                {
                    week.ElementAt(dayWithMaxAmountOfClasses - 1).WasVisitedOnPWeek = true;
                }
                else
                {
                    week.ElementAt(dayWithMaxAmountOfClasses - 1).WasVisitedOnNWeek = true;
                }

            } while (iter < week.Count() * 2);

            OptimizeCreatedPlan();
            OptimizeCreatedPlan();
        }

        private void OptimizeCreatedPlan()
        {
            var classesToAdd = new List<ClassObj>();
            var classesToRemove = new List<ClassObj>();
            bool ifSomethingWasAdded = false;

            foreach (ClassObj checkingClass in CreatedPlan)
            {
                var earlierHour = HourToIntConverter.ConvertToInt(CreatedPlan.Where(a => a.Day == checkingClass.Day
                        && (a.WhichWeek == checkingClass.WhichWeek || a.WhichWeek == "Co tydzien")).Min(b => b.StartHour));
                var latestHour = HourToIntConverter.ConvertToInt(CreatedPlan.Where(a => a.Day == checkingClass.Day
                        && (a.WhichWeek == checkingClass.WhichWeek || a.WhichWeek == "Co tydzien")).Max(b => b.EndHour));

                if ((LookForClassesLaterThen(checkingClass).Count != 2
                    && (LookForClassesLaterThen(checkingClass).Count == 1 && LookForClassesLaterThen(checkingClass).First().WhichWeek != "Co tydzien"))
                    || LookForClassesLaterThen(checkingClass).Count == 0)
                {
                    var listSomeClasses = AllClasses.Where(a => a.CourseCode == checkingClass.CourseCode).ToList();
                    listSomeClasses.Remove(checkingClass);

                    foreach (var lookForBetter in listSomeClasses)
                    {
                        if (!IsClassCollidingWithOther(lookForBetter)
                            && HourToIntConverter.ConvertToInt(lookForBetter.StartHour) > earlierHour
                            && HourToIntConverter.ConvertToInt(lookForBetter.StartHour) < latestHour
                            && !IsChangeCosmetical(checkingClass, lookForBetter)
                            && (IfSomethingIsEarlier(lookForBetter).Count == 2
                                || (IfSomethingIsEarlier(lookForBetter).Count == 1
                                   && (IfSomethingIsEarlier(lookForBetter).First().WhichWeek == lookForBetter.WhichWeek
                                      || IfSomethingIsEarlier(lookForBetter).First().WhichWeek == "Co tydzien"))))
                        {
                            classesToRemove.Add(checkingClass);
                            classesToAdd.Add(lookForBetter);
                            ifSomethingWasAdded = true;
                            break;
                        }
                    }
                }

                if ((IfSomethingIsEarlier(checkingClass).Count != 2
                    && (IfSomethingIsEarlier(checkingClass).Count == 1 && IfSomethingIsEarlier(checkingClass).First().WhichWeek != "Co tydzien"))
                    || IfSomethingIsEarlier(checkingClass).Count == 0
                    && !ifSomethingWasAdded)
                {
                    var listSomeClasses = AllClasses.Where(a => a.CourseCode == checkingClass.CourseCode).ToList();
                    listSomeClasses.Remove(checkingClass);

                    foreach (ClassObj lookForBetter in listSomeClasses)
                    {
                        if (!IsClassCollidingWithOther(lookForBetter)
                            && HourToIntConverter.ConvertToInt(lookForBetter.StartHour) > earlierHour
                            && HourToIntConverter.ConvertToInt(lookForBetter.StartHour) < latestHour
                            && !IsChangeCosmetical(checkingClass, lookForBetter)
                            && (IfSomethingIsEarlier(lookForBetter).Count == 2
                                || (IfSomethingIsEarlier(lookForBetter).Count == 1
                                   && (IfSomethingIsEarlier(lookForBetter).First().WhichWeek == lookForBetter.WhichWeek
                                      || IfSomethingIsEarlier(lookForBetter).First().WhichWeek == "Co tydzien"))))
                        {
                            classesToRemove.Add(checkingClass);
                            classesToAdd.Add(lookForBetter);
                            break;
                        }
                    }
                }
                ifSomethingWasAdded = false;
            }

            foreach (var classToRemove in classesToRemove)
            {
                CreatedPlan.Remove(classToRemove);
            }

            CreatedPlan.AddRange(classesToAdd);
        }

        private List<ClassObj> IfSomethingIsEarlier(ClassObj checkingClass)
        {
            var listOfPrevClasses = new List<ClassObj>();
            var listOfClassesInTheSameDay = CreatedPlan.Where(a => a.Day == checkingClass.Day).ToList();

            foreach (var classAtSameDay in listOfClassesInTheSameDay)
            {
                if (CompareHours(checkingClass.StartHour, classAtSameDay.EndHour) <= 30)
                    listOfPrevClasses.Add(classAtSameDay);
            }
            return listOfPrevClasses;
        }

        private List<ClassObj> LookForClassesLaterThen(ClassObj checkingClass)
        {
            var listOfClassesInTheSameDay = CreatedPlan.Where(a => a.Day == checkingClass.Day).ToList();

            return listOfClassesInTheSameDay.Where(classObj => CompareHours(checkingClass.EndHour, classObj.StartHour) <= 30).ToList();
        }

        private bool IsChangeCosmetical(ClassObj checkingClass, ClassObj lookForBetter)
        {
            if(checkingClass.Day == lookForBetter.Day && checkingClass.StartHour == lookForBetter.StartHour)
            {
                return true;
            }

            return false;
        }

        public static int CompareHours(string firstH, string secondH)
        {
            var firstHourRaw = firstH.Split(':');
            var secondHourRaw = secondH.Split(':');
            var firstHourInInt = new[] { int.Parse(firstHourRaw[0]), int.Parse(firstHourRaw[1]) };
            var secondHourInInt = new[] { int.Parse(secondHourRaw[0]), int.Parse(secondHourRaw[1]) };

            var diffH = (secondHourInInt[0] - firstHourInInt[0]) * 60;
            int diffM;
            if (diffH < 0)
            {
                diffH = Math.Abs(diffH);
                diffM = (firstHourInInt[1] - secondHourInInt[1]);
            }
            else
            {
                diffM = (secondHourInInt[1] - firstHourInInt[1]);
            }

            return Math.Abs(diffH + diffM);
        }

        public List<ClassObj> GetSorted()
        {
            return CreatedPlan;
        }

        #endregion

        private class TheDay
        {
            public int Id { get; set; }
            public bool WasVisitedOnNWeek { get; set; }
            public bool WasVisitedOnPWeek { get; set; }
        }
    }
}