﻿using System.Collections.Generic;
using Planner.Data;

namespace Planner.Model
{
    internal interface IAlgorithm
    {
        void Sort();
        List<ClassObj> GetSorted();
    }
}