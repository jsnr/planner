﻿using System;
using System.Collections.Generic;
using System.Linq;
using Planner.Data;

namespace Planner.Model
{
    internal class AlgorithmSortAsEarlyAsPossible : BaseSort, IAlgorithm
    {
        #region Constructor(s)

        public AlgorithmSortAsEarlyAsPossible(List<ClassObj> classes) :
            base(classes)
        {
        }

        #endregion

        #region Methods
        
        public void Sort()
        {
            var isSomethingWasAdded = false;

            do
            {
                var firstElement = Courses.First();
                var latestStartHour =
                    AllClasses.Where(c => c.CourseCode == firstElement.CourseCode).Min(h => h.StartHour);
                var itemToAdd =
                    AllClasses.FirstOrDefault(
                        c => c.CourseCode == firstElement.CourseCode && c.StartHour == latestStartHour);

                if (!IsClassCollidingWithOther(itemToAdd))
                {
                    isSomethingWasAdded = true;
                    CreatedPlan.Add(itemToAdd);
                    DeleteCourse(firstElement);
                }
                else
                {
                    foreach (var singleClass in AllClasses.Where(c => c.CourseCode == itemToAdd.CourseCode).ToList())
                    {
                        if (!IsClassCollidingWithOther(singleClass))
                        {
                            CreatedPlan.Add(singleClass);
                            isSomethingWasAdded = true;
                            DeleteCourse(firstElement);
                            break;
                        }
                    }
                }

                if (!isSomethingWasAdded)
                {
                    var listOfCollidingItems = FindCollidingClasses(itemToAdd);
                    foreach (var classObj in listOfCollidingItems)
                    {
                        Courses.Insert(0, new CoursesObj {CourseCode = classObj.CourseCode});
                        CreatedPlan.RemoveAll(c => c.CourseCode == classObj.CourseCode);
                    }
                }

                isSomethingWasAdded = false;
            } while (Courses.Count != 0);
        }

        public List<ClassObj> GetSorted()
        {
            return CreatedPlan;
        }

        #endregion
    }
}