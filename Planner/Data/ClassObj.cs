﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Planner.Data
{
    [Serializable]
    public class Classes
    {
        public Classes() { Class = new List<ClassObj>(); }

        [XmlArray("ClassList"), XmlArrayItem(typeof(ClassObj), ElementName = "ClassObj")]
        public List<ClassObj> Class { get; set; }
    }

    [Serializable]
    [XmlRoot("AllClasses")]
    public class ClassObj
    {
        public string GroupCode { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }
        public string TeacherName { get; set; }
        public string RoomNumber { get; set; }
        public int Day { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public string WhichWeek { get; set; }
    }
}
