﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Data
{
    public class AppConfigObj
    {
        public bool IsThreadingEnabled { get; set; }

        public string PathToSave { get; set; }
    }
}
