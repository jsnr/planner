﻿using System.Collections.Generic;

namespace Planner.Data
{
    internal class CoursesObj
    {
        public string CourseCode { get; set; }
        public int CourseOccurenceInPlan { get; set; }
    }
}