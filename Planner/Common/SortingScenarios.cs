﻿using System.ComponentModel;

namespace Planner.Common
{
    public enum SortingScenarios
    {
        [Description("Jak najwcześniej")]
        AsEarlyAsPossible,
        AsLateAsPossible,
        ManyDaysFree,
        NoBreaks
    }
}
